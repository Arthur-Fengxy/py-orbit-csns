//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//   LostParticleTime.cc
//
// AUTHOR
//   Xiaohan Lu
//
// CREATED
//    04/21/2020
//
// DESCRIPTION
//    A subclass of a ParticleAttributes class
//
//
///////////////////////////////////////////////////////////////////////////

#include "Bunch.hh"
#include "LostParticleTime.hh"

LostParticleTime::LostParticleTime(Bunch* bunch): 
ParticleAttributes(bunch,1)
{
  cl_name_ = "LostParticleTime";
  attrDescr = "Time(s)";

}

LostParticleTime::~LostParticleTime()
{
}

double& LostParticleTime::getTime(int particle_index){
	return attValue(particle_index,0);
}
