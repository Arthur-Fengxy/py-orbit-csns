#ifndef WRAP_ORBIT_COLLIMATORNEW_HH_
#define WRAP_ORBIT_COLLIMATORNEW_HH_

#include "Python.h"

#ifdef __cplusplus
extern "C" {
#endif

  namespace wrap_collimatorNew{
    void initcollimatorNew();
  }

#ifdef __cplusplus
}
#endif

#endif /*WRAP_ORBIT_COLLIMATORNEW_HH_*/
